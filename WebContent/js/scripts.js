// created by ctrlz.tech

let clearButtonAll = document.createElement("button");
clearButtonAll.innerHTML = "Clear search";
clearButtonAll.addEventListener('click', function () {
    document.getElementById('empShowAll').innerHTML = ''
    document.getElementById('empFindAll').removeChild(clearButtonAll);
});

let clearButtonId = document.createElement("button");
clearButtonId.innerHTML = "Clear Search";
clearButtonId.addEventListener('click', function () {
    document.getElementById('empShowById').innerHTML = ''
    document.getElementById('empFindById').removeChild(clearButtonId);
});

let clearButtonUpd = document.createElement("button");
clearButtonUpd.innerHTML = "Clear Search";
clearButtonUpd.addEventListener('click', function () {
    document.getElementById('empUpdateById').innerHTML = ''
    document.getElementById('empFindUpdateById').removeChild(clearButtonUpd);
    document.getElementById('empFindUpdateById').removeChild(updateEmpButton);
});

let updateEmpButton = document.createElement("button");
updateEmpButton.innerHTML = "Update Employee Fields!";
// updateEmpButton.addEventListener('click', function () {
//     var idOfEmpToUpdate = document.getElementById('emp-to-be-updated-id').innerHTML;
//     alert('You are about to update employee with ID of: ' + idOfEmpToUpdate + ' with the selected fields!');
// });

let clearButtonDel = document.createElement("button");
clearButtonDel.innerHTML = "Clear Search";
clearButtonDel.addEventListener('click', function () {
    document.getElementById('empDelById').innerHTML = ''
    document.getElementById('empDeleteById').removeChild(clearButtonDel);
    document.getElementById('empDeleteById').removeChild(deleteEmpButton);
});

let deleteEmpButton = document.createElement("button");
deleteEmpButton.innerHTML = "Delete Employee!";
// deleteEmpButton.addEventListener('click', function() {
//     let idOfEmpToDelete = document.getElementById('emp-to-be-deleted-id').innerHTML;
//     let nameOfEmpToDelete = document.getElementById('emp-to-be-deleted-name').innerHTML
//     //alert('You are about to delete employee ' + nameOfEmpToDelete + ' with id of: ' + idOfEmpToDelete);
//     //confirm();
// });


// const addEmpBtn = document.querySelector("#add-emp-btn");
// const updateEmpBtn;
// const deleteEmpBtn;



document.getElementById('find-all').addEventListener('click', function () {
    // AJAX request to EmployeeServletAll

    $.ajax({
        type: "GET",
        url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
        dataType: "json",
        //contentType: "application/json;charset=iso-8859-1",
        success: function (data) {
            console.log('DB data: ', data)

            let table = `<table border="1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Level</th>
                        <th>Employment date</th>
                        <th>Birthday</th>
                    </tr>
                </thead>
                <tbody>
            `;
            for (let e of data) {
                table += `<tr><td>${e.id}</td><td>${e.name}</td><td>${e.address}</td><td>${e.experience}</td><td>${e.dateJoin}</td><td>${e.birthday}</td></tr>`;
            }
            table += `</tbody>
            </table>`;
            document.getElementById('empShowAll').innerHTML = table;
            console.log("find all emps clicked", data);

            document.getElementById('empFindAll').appendChild(clearButtonAll);



        }
    });
})

document.getElementById('btn-find-by-id').addEventListener('click', function () {
    let id = document.getElementById('id-of-emp-to-find').value;
    console.log('finding employee with id: ', id);

    $.ajax({
        type: "GET",
        url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
        dataType: "json",
        success: function (data) {
            console.log('DB data: ', data)

            let table = `<table border="1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Level</th>
                        <th>Employment date</th>
                        <th>Birthday</th>
                    </tr>
                </thead>
                <tbody>
            `;

            for (let e of data) {
                if (id == e.id) {

                    table += `<tr><td>${e.id}</td><td>${e.name}</td><td>${e.address}</td><td>${e.experience}</td><td>${e.dateJoin}</td><td>${e.birthday}</td></tr>`;
                }
            }
            table += `</tbody>
                </table>`;



            document.getElementById('empShowById').innerHTML = table;
            console.log("find all emps clicked", data);

            document.getElementById('empFindById').appendChild(clearButtonId);

        }
    });

})

document.getElementById('add-emp-btn').addEventListener('click', function () {
    console.log("adding new employee...")

    let newEmpName = $("#new-emp-name").val();
    let newEmpAddress = $("#new-emp-address").val();
    let newEmpLevel = $("#new-emp-level").val();
    let newEmpBirthday = $("#new-emp-birthday").val();


    if (newEmpLevel < 1 || newEmpLevel > 10) {
        alert('Please input a level between 1 and 10');
        return;
    }

    console.log('1. we will send: ', newEmpName, newEmpAddress, newEmpLevel, newEmpBirthday);

    var objectToSend = {
        name: newEmpName,
        address: newEmpAddress,
        level: newEmpLevel,
        birthday: newEmpBirthday
    };

    console.log('2. sending data: ', objectToSend);


    $.ajax({
        url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
        method: 'post',
        type: 'POST',
        dataType: 'json',
        data: JSON.stringify(objectToSend),

        success: function (objectReceived) {
            if (objectReceived.status == "ok") {
                console.log('3. server replied (OK): ', objectReceived);
            }

        },
        error: function (objectReceived) {
            if (objectReceived.status == "fail") {
                console.log('3. server replied (FAIL): ', objectReceived);
                console.log("DOPOST ERROR on the servlet");
            }

        } // DE CE nu apar console logurile din functiile success SAU error??? 
        // => response.getWriter().append(jsonObjectResponse.toString());


    });
})

document.getElementById('btn-find-upd-by-id').addEventListener('click', function () {
    let id = document.getElementById('id-of-emp-to-find-upd').value;
    console.log('finding employee with id: ', id);

    $.ajax({
        type: "GET",
        url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
        dataType: "json",
        success: function (data) {
            console.log('DB data: ', data)

            let table = `<table border="1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name (click below to edit)</th>
                        <th>Address (click below to edit)</th>
                        <th>Level (click below to edit)</th>
                        <th>Employment date</th>
                        <th>Birthday</th>
                    </tr>
                </thead>
                <tbody>
            `;

            for (let e of data) {
                if (id == e.id) {

                    table += `<tr><td id = "emp-to-be-updated-id">${e.id}</td><td id = "emp-to-be-updated-name" contenteditable='true' bgcolor = "white">${e.name}</td><td id = "emp-to-be-updated-address" contenteditable='true' bgcolor = "white">${e.address}</td><td id = "emp-to-be-updated-level" contenteditable='true' bgcolor = "white">${e.experience}</td><td>${e.dateJoin}</td><td>${e.birthday}</td></tr>`;
                }
            }
            table += `</tbody>
                </table>`;



            document.getElementById('empUpdateById').innerHTML = table;
            console.log("find all emps clicked", data);

            document.getElementById('empFindUpdateById').appendChild(clearButtonUpd);
            document.getElementById('empFindUpdateById').appendChild(updateEmpButton);

            updateEmpButton.classList.add("right");
            clearButtonUpd.style.display = "block";
            updateEmpButton.style.display = "block";

            // elegant sollution to addEventListener stacking on top of each other:
            updateEmpButton.setAttribute('data-id', id);

        }
    });

})

updateEmpButton.addEventListener('click', function () {
    let id = this.getAttribute('data-id');
    console.log('updating employee with id: ' + id);

    if (id == "" || id == null) {
        updateEmpButton.style.display = "none";
        alert('Please input a valid ID!');
        return;
    }

    let empName = document.getElementById('emp-to-be-updated-name');
    let empAddress = document.getElementById('emp-to-be-updated-address');
    let empLevel = document.getElementById('emp-to-be-updated-level');
    let newName = empName.textContent || empName.innerText;
    let newAddress =  empAddress.textContent || empAddress.innerText;
    let newLevel =  empLevel.textContent || empLevel.innerText;


    // const nameWHITESPACE = newName => console.log(/^(?:nbsp;)+$/.nameWHITESPACE(newName));
    // const addressWHITESPACE = newAddress => console.log(/^(?:nbsp;)+$/.addressWHITESPACE(newAddress));

    // if(nameWHITESPACE){
    //     updateEmpButton.style.display = "none";
    //     alert('Please input a valid NAME!');
    //     return;
    // }
    // if(addressWHITESPACE){
    //     updateEmpButton.style.display = "none";
    //     alert('Please input a valid ADRESS!');
    //     return;
    // }
    // https://stackoverflow.com/questions/54702064/javascript-check-if-string-contains-only-nbsp
    // how to check ONLY for whitespace?
    // WIP!

    newName = newName.replaceAll('&nbsp;', ' ').trim();
    console.log('nn: ', newName);
    newName = newName.replace(/\s+/g, ' ').trim();
    // s+ => AT LEAST ONE space character
    console.log('nn2: ', newName);
    // let debug = true;
    // if(debug){
    //     return;
    // }

    // check for “Non-breaking space”
    if (newName == String.fromCharCode(160)) {
        updateEmpButton.style.display = "none";
        alert('Please input a valid NAME!');
        return;
    }
    // check for “Non-breaking space”
    if (newAddress == String.fromCharCode(160)) {
        updateEmpButton.style.display = "none";
        alert('Please input a valid ADDRESS!');
        return;
    }
    if (newName.length < 3 || newName.trim() < 1 || newName.valueOf() == '&nbsp;' || newName.trim() == "" || newName == null || newName.trim() == " ") {
        updateEmpButton.style.display = "none";
        alert('Please input a valid NAME!');
        return;
    }
    if (newAddress.length < 3 || newAddress.trim() < 1 || newAddress.valueOf() == '&nbsp;' || newAddress.trim() == "" || newAddress == null || newAddress.trim() == " ") {
        updateEmpButton.style.display = "none";
        alert('Please input a valid ADDRESS!');
        return;
    }

    console.log('1. we will send, ID: ' + id + ', Name :' + newName + ', Address: ' + newAddress + ', Level: ' + newLevel);

    var objectToSend = {
        idOfEmpToUpdate: id,
        newEmpName: newName,
        newEmpAddress: newAddress,
        newEmpLevel: newLevel,
    };

    console.log('2. sending data: ', objectToSend);

    $.ajax({
        url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
        method: 'put',
        type: 'PUT',
        dataType: 'json',
        data: JSON.stringify(objectToSend),


        success: function (objectReceived) {
            if (objectReceived.status == "ok") {
                console.log('3. server replied (OK): ', objectReceived);
                console.log('4. You have updated an employee');
            }

        },
        error: function (objectReceived) {
            if (objectReceived.status == "fail") {
                console.log('3. server replied (FAIL): ', objectReceived);
                console.log("4. DOPUT ERROR on the servlet");
                console.log('5. Something went wrong while updating an employee ');
            }

        }
    });
    document.getElementById('empUpdateById').innerHTML = ''
    clearButtonUpd.style.display = "none";
    updateEmpButton.style.display = "none";
})

document.getElementById('btn-find-delete-by-id').addEventListener('click', function () {

    let id = document.getElementById('id-of-emp-to-find-del').value;


    $.ajax({
        type: "GET",
        url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
        dataType: "json",
        success: function (data) {
            console.log('DB data: ', data)

            let table = `<table border="1">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Level</th>
                        <th>Employment date</th>
                        <th>Birthday</th>
                    </tr>
                </thead>
                <tbody>
            `;

            for (let e of data) {
                if (id == e.id) {

                    table += `<tr><td id = "emp-to-be-deleted-id">${e.id}</td><td id = "emp-to-be-deleted-name">${e.name}</td><td>${e.address}</td><td>${e.experience}</td><td>${e.dateJoin}</td><td>${e.birthday}</td></tr>`;
                }
            }
            table += `</tbody>
                </table>`;



            document.getElementById('empDelById').innerHTML = table;
            console.log("find all emps clicked", data);

            document.getElementById('empDeleteById').appendChild(clearButtonDel);
            document.getElementById('empDeleteById').appendChild(deleteEmpButton);

            deleteEmpButton.classList.add("right");
            clearButtonDel.style.display = "block";
            deleteEmpButton.style.display = "block";

            // elegant sollution to addEventListener stacking on top of each other:
            deleteEmpButton.setAttribute('data-id', id);





        }

    });


})

deleteEmpButton.addEventListener('click', function () {

    let id = this.getAttribute('data-id');
    //var id = document.getElementById('id-of-emp-to-find-del').value;
    console.log('deleting employee with id: ', id);

    // return;
    //let empToDeleteId = $("#emp-to-be-deleted-id").val();
    if (id == "" || id == null) {
        deleteEmpButton.style.display = "none";
        alert('Please input a value!');
        return;
    }
    console.log('1. we will send: ID: ', id);

    var objectToSend = {
        idOfEmpToDelete: id,
    };

    console.log('2. sending data: ', objectToSend);

    $.ajax({
        url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
        method: 'delete',
        type: 'DELETE',
        dataType: 'json',
        data: JSON.stringify(objectToSend),


        success: function (objectReceived) {
            if (objectReceived.status == "ok") {
                console.log('3. server replied (OK): ', objectReceived);
                console.log('4. You have deleted an employee');
            }

        },
        error: function (objectReceived) {
            if (objectReceived.status == "fail") {
                console.log('3. server replied (FAIL): ', objectReceived);
                console.log("4. DODELETE ERROR on the servlet");
                console.log('5. Something went wrong while deleting an employee ');
            }

        }
    });
    document.getElementById('empDelById').innerHTML = ''
    clearButtonDel.style.display = "none";
    deleteEmpButton.style.display = "none";
})


// function findAllEmployees() {
//     // request to EmployeeServletAll

//     $.ajax({
//         type: "GET",
//         url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
//         dataType: "json",
//         success: function (data) {
//             console.log('DB data: ', data)

//             let table = `<table border="1">
//                 <thead>
//                     <tr>
//                         <th>ID</th>
//                         <th>Name</th>
//                         <th>Address</th>
//                         <th>Level</th>
//                         <th>Employment date</th>
//                         <th>Birthday</th>
//                     </tr>
//                 </thead>
//                 <tbody>
//             `;
//             for (let e of data) {
//                 table += `<tr><td>${e.id}</td><td>${e.name}</td><td>${e.address}</td><td>${e.experience}</td><td>${e.dateJoin}</td><td>${e.birthday}</td></tr>`;
//             }
//             table += `</tbody>
//             </table>`;
//             document.getElementById('empShowAll').innerHTML = table;
//             console.log("find all emps clicked", data);

//             document.getElementById('empFindAll').appendChild(clearButtonAll);



//         }
//     });


// }

// function addNewEmployee() {
//     console.log("adding new employee...")

//     let newEmpName = $("#new-emp-name").val();
//     let newEmpAddress = $("#new-emp-address").val();
//     let newEmpLevel = $("#new-emp-level").val();
//     let newEmpBirthday = $("#new-emp-birthday").val();


//     if(newEmpLevel < 1 || newEmpLevel > 10){
//         alert('Please input a level between 1 and 10');
//         return;
//     }

//     console.log('1. we will send: ', newEmpName, newEmpAddress, newEmpLevel, newEmpBirthday);

//     var objectToSend = {
//         name: newEmpName,
//         address: newEmpAddress,
//         level: newEmpLevel,
//         birthday: newEmpBirthday
//     };

//     console.log('2. sending data: ', objectToSend);


//     $.ajax({
//         url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
//         method: 'post',
//         type: 'POST',
//         dataType: 'json',
//         data: JSON.stringify(objectToSend),

//         success: function (objectReceived) {
//             if (objectReceived.status == "ok") {
//                 console.log('3. server replied (OK): ', objectReceived);
//             }

//         },
//         error: function (objectReceived) {
//             if (objectReceived.status == "fail") {
//                 console.log('3. server replied (FAIL): ', objectReceived);
//                 console.log("DOPOST ERROR on the servlet");
//             }

//         } // DE CE nu apar console logurile din functiile success SAU error??? 
//         // => response.getWriter().append(jsonObjectResponse.toString());


//     });

// }



// function confirm(){
//     let deleteEmpButtonConfirm = document.createElement("button");
//             document.getElementById('empDeleteById').appendChild(deleteEmpButtonConfirm);
//             deleteEmpButtonConfirm.innerHTML = "Confirm deletion";
//             deleteEmpButtonConfirm.classList.add("right");
//             deleteEmpButtonConfirm.id = "emp-delete-confirm";
// }

// deleteEmpButtonConfirm.addEventListener('click', function(){

//     console.log('finding employee with id: ', id);

//     let empToDeleteId = $("#emp-to-be-deleted-id").val();

//     console.log('1. we will send: ', empToDeleteId);

//     var objectToSend = { 
//         id : empToDeleteId,
//     };

//     console.log('2. sending data: ', objectToSend);


//         $.ajax({
//             url: "http://localhost:8080/EmployeeRegisterJDBC/EmployeeServletAll",
//             method: 'delete',
//             type: 'DELETE',
//             dataType: 'json',
//             data: JSON.stringify(objectToSend),




//             success: function (objectReceived) {
//                 if(objectReceived.status == "ok"){
//                     console.log('3. server replied (OK): ', objectReceived);
//                     alert('You have deleted employee ' + nameOfEmpToDelete + ' with id of: ' + idOfEmpToDelete);
//                 }

//             },
//             error: function (objectReceived) {
//                 if(objectReceived.status == "fail"){
//                     console.log('3. server replied (FAIL): ', objectReceived);
//                     console.log("DODELETE ERROR on the servlet");
//                     alert('Something went wrong while deleting employee ' + nameOfEmpToDelete + ' with id of: ' + idOfEmpToDelete);
//                 }

//             } 
//         });

// })
