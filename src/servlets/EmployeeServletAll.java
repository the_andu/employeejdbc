package servlets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import dao.DaoEmployee;
import model.Employee;

@WebServlet("/EmployeeServletAll")
public class EmployeeServletAll extends HttpServlet {

	private DaoEmployee dao = new DaoEmployee();
	List<Employee> employees = dao.findAllEmployees();

	// serves the response for findAllEmployees() and findById(n)
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		JSONArray result = new JSONArray();

		for (Employee e : employees) {
			System.out.println(
					"Employees in DB: " + e.getEmployeeId() + ", " + e.getDateOfJoining() + ", " + e.getEmployeeName()
							+ ", " + e.getEmployeeAddress() + ", " + e.getExperience() + ", " + e.getDateOfBirth());
			JSONObject json = new JSONObject();
			json.append("Employees in DB: ", e); // toString -> NOT JSON
			System.out.println("JSON Emps: " + json);

			JSONObject jsonE = new JSONObject();
			jsonE.put("id", e.getEmployeeId());
			jsonE.put("name", e.getEmployeeName());
			jsonE.put("address", e.getEmployeeAddress());
			jsonE.put("experience", e.getExperience());
			jsonE.put("dateJoin", e.getDateOfJoining());
			jsonE.put("birthday", e.getDateOfBirth());

			result.put(jsonE);
		}

		request.setAttribute("DB_EMPS", employees);

		response.getWriter().append(result.toString());
	}
	
	// adds new employee(s) to DB
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String newEmployeeString = br.readLine();
		System.out.println("new employee STRING: " + newEmployeeString);

		JSONObject jsonObjectRequest = new JSONObject(newEmployeeString);
		System.out.println("new employee JSON: " + jsonObjectRequest);

		JSONObject jsonObjectResponse = new JSONObject();

		String newEmpName = jsonObjectRequest.getString("name");
		String newEmpAddress = jsonObjectRequest.getString("address");
		Integer newEmpLevel = (Integer.valueOf(jsonObjectRequest.getString("level")));

		if (newEmpLevel < 1 || newEmpLevel > 10) {
			throw new RuntimeException("Invalid employee level!");
		}
		if (jsonObjectRequest.getString("birthday").length() > 1) {
			try {
				Date newEmpBirthday = Date.valueOf(jsonObjectRequest.getString("birthday"));
				Employee newEmployee = new Employee(newEmpName, newEmpAddress, newEmpLevel, newEmpBirthday);
				dao.save(newEmployee);
				jsonObjectResponse.put("status", "ok");
				System.out.println("(SERVLET) DO POST: New employee added to database!");
			} catch (JSONException e) {
				jsonObjectResponse.put("status", "fail");
				e.printStackTrace();
			}
		} else {
			try {
				Date newEmpBirthday = null;
				Employee newEmployee = new Employee(newEmpName, newEmpAddress, newEmpLevel, newEmpBirthday);
				dao.save(newEmployee);
				jsonObjectResponse.put("status", "ok");
			} catch (JSONException e) {
				jsonObjectResponse.put("status", "fail");
				e.printStackTrace();
			}
		}

		response.getWriter().append(jsonObjectResponse.toString());
	}

	// deletes employee(s) from DB
	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String idOfEmpToDeleteString = br.readLine();
		System.out.println("ID (string) of employee to delete is: " + idOfEmpToDeleteString);

		JSONObject jsonObjectRequest = new JSONObject(idOfEmpToDeleteString);
		System.out.println("JSON of employee to delete is: " + jsonObjectRequest);

		/*
		 * if ((jsonObjectRequest.getString("idOfEmpToDelete")) == "" ||
		 * (Integer.valueOf(jsonObjectRequest.getString("idOfEmpToDelete"))) == null) {
		 * throw new NumberFormatException("Invalid input from frontend!"); }
		 */
		JSONObject jsonObjectResponse = new JSONObject();
		try {
			Integer idOfEmpToDeleteInteger = (Integer.valueOf(jsonObjectRequest.getString("idOfEmpToDelete")));

			System.out.println("ID (integer) of employee to delete is: " + idOfEmpToDeleteInteger);

			if ((jsonObjectRequest.getString("idOfEmpToDelete").length() >= 1)) {
				try {
					Employee empToDelete = dao.findById(idOfEmpToDeleteInteger);
					System.out.println("Employee to delete is: " + empToDelete);
					if (empToDelete.getEmployeeId() != null) {
						dao.deleteById(idOfEmpToDeleteInteger);
						jsonObjectResponse.put("status", "ok");
						System.out.println("(SERVLET) DO DELETE: Employee deleted from database!");
					}
				} catch (NumberFormatException e) {
					jsonObjectResponse.put("status", "fail");
					response.getWriter().append(jsonObjectResponse.toString());
					e.printStackTrace();
				} catch (JSONException e) {
					jsonObjectResponse.put("status", "fail");
					response.getWriter().append(jsonObjectResponse.toString());
					e.printStackTrace();
				}

			}
		} catch (NumberFormatException e) {
			System.out.println("Invalid input from frontend!");
			jsonObjectResponse.put("status", "fail");
			response.getWriter().append(jsonObjectResponse.toString());
			e.printStackTrace();
		} catch (JSONException e) {
			jsonObjectResponse.put("status", "fail");
			response.getWriter().append(jsonObjectResponse.toString());
			e.printStackTrace();
		}
		response.getWriter().append(jsonObjectResponse.toString());
		// br.close();
		// best practice in cleaning resources ?
	}

	// updates employee(s) record(s)
	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream()));
		String newEmployeeString = br.readLine();
		System.out.println("new employee STRING: " + newEmployeeString);

		JSONObject jsonObjectRequest = new JSONObject(newEmployeeString);
		System.out.println("new employee JSON: " + jsonObjectRequest);

		JSONObject jsonObjectResponse = new JSONObject();

		String newEmpName = jsonObjectRequest.getString("newEmpName");
		String newEmpAddress = jsonObjectRequest.getString("newEmpAddress");
		Integer newEmpLevel = (Integer.valueOf(jsonObjectRequest.getString("newEmpLevel")));
		Integer id = jsonObjectRequest.getInt("idOfEmpToUpdate");

		Employee empToUpdate = new Employee();

		if (id < 0) {
			jsonObjectResponse.put("status", "fail");
			response.getWriter().append(jsonObjectResponse.toString());
			try {
				throw new Exception("Invalid input from frontend! (ID)");
			} catch (Exception e) {
				System.out.println("Invalid input from frontend! (ID)");
				jsonObjectResponse.put("status", "fail");
				response.getWriter().append(jsonObjectResponse.toString());
				e.printStackTrace();
				return;
			}
		}
		if (newEmpLevel < 1 || newEmpLevel > 10) {
			jsonObjectResponse.put("status", "fail");
			response.getWriter().append(jsonObjectResponse.toString());
			try {
				throw new Exception("Invalid input from frontend! (LEVEL)");
			} catch (Exception e) {
				System.out.println("Invalid input from frontend! (LEVEL)");
				jsonObjectResponse.put("status", "fail");
				response.getWriter().append(jsonObjectResponse.toString());
				e.printStackTrace();
				return;
			}
		}
		if (newEmpName.length() < 3 || newEmpAddress.length() < 3
				|| newEmpName.isEmpty() || newEmpAddress.isEmpty()
				|| newEmpName.trim().isEmpty() || newEmpAddress.trim().isEmpty()
				|| newEmpName.trim().length() < 1 || newEmpAddress.trim().length() < 1
				|| newEmpName.equals("&nbsp;") || newEmpAddress.equals("&nbsp;")
				|| newEmpName.equals(" ") || newEmpAddress.equals(" ")
				|| newEmpName.equals(null) || newEmpAddress.equals(null) 
				|| newEmpName.equals("") || newEmpAddress.equals("")) {
			jsonObjectResponse.put("status", "fail");
			response.getWriter().append(jsonObjectResponse.toString());
			try {
				throw new Exception("Invalid input from frontend! (NAME or ADDRESS)");
			} catch (Exception e) {
				System.out.println("Invalid input from frontend! (NAME or ADDRESS)");
				jsonObjectResponse.put("status", "fail");
				response.getWriter().append(jsonObjectResponse.toString());
				e.printStackTrace();
				return;
			}
		}
		try {
			empToUpdate.setEmployeeId(id);
			empToUpdate.setEmployeeAddress(newEmpAddress);
			empToUpdate.setEmployeeName(newEmpName);
			empToUpdate.setExperience(newEmpLevel);
			dao.updateById(empToUpdate);
			jsonObjectResponse.put("status", "ok");
			System.out.println("(SERVLET) DO PUT: Updated employee is: !" + empToUpdate);
		} catch (JSONException e) {
			jsonObjectResponse.put("status", "fail");
			response.getWriter().append(jsonObjectResponse.toString());
			e.printStackTrace();
			return;
		}
		response.getWriter().append(jsonObjectResponse.toString());
	}

}
