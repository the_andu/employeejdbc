package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import model.Employee;

public class DaoEmployee implements IDao<Employee> {

	@Override // SELECT * FROM employees
	public List<Employee> findAllEmployees() {
		List<Employee> empsInDb = new ArrayList<>();

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeeregister", "root",
					"1234");

			Statement stmt = con.createStatement();
			ResultSet tableArray = stmt.executeQuery("SELECT * FROM employees");

			while (tableArray.next()) {
				System.out.println("ID: " + tableArray.getInt("employee_id"));
				System.out.println("Employee Name: " + tableArray.getString("employee_name"));

				Employee emp = new Employee();
				emp.setEmployeeId(tableArray.getInt("employee_id"));
				emp.setEmployeeName(tableArray.getString("employee_name"));
				emp.setEmployeeAddress(tableArray.getString("employee_address"));
				emp.setExperience(tableArray.getInt("employee_level"));
				// yyyy-MM-dd
				SimpleDateFormat sdfHourMinutesSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				
				String empJoinDateStr = tableArray.getString("employment_date");
				Date empJoinDate = sdfHourMinutesSeconds.parse(empJoinDateStr);
				emp.setDateOfJoining(empJoinDate);

				String empBirthDateStr = tableArray.getString("employee_birthday");
				if (empBirthDateStr != null) {
					Date dob = sdf.parse(empBirthDateStr);
					emp.setDateOfBirth(dob);
				}

				empsInDb.add(emp);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return empsInDb;
	}

	@Override
	public Employee findById(Integer employeeId) {
		Employee emp = new Employee();

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeeregister", "root",
					"1234");

			Statement stmt = con.createStatement();
			ResultSet empById = stmt.executeQuery("SELECT * FROM employees WHERE employee_id = " + employeeId);

			while (empById.next()) {

				emp.setEmployeeId(empById.getInt("employee_id"));
				emp.setEmployeeName(empById.getString("employee_name"));
				emp.setEmployeeAddress(empById.getString("employee_address"));
				emp.setExperience(empById.getInt("employee_level"));
				// yyyy-MM-dd
				SimpleDateFormat sdfHourMinutesSeconds = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				
				String empJoinDateStr = empById.getString("employment_date");
				Date empJoinDate = sdfHourMinutesSeconds.parse(empJoinDateStr);
				emp.setDateOfJoining(empJoinDate);

				String empBirthDateStr = empById.getString("employee_birthday");
				if (empBirthDateStr != null) {
					Date dob = sdf.parse(empBirthDateStr);
					emp.setDateOfBirth(dob);
				}

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return emp;

	}

	@Override
	public void save(Employee newElement) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeeregister", "root", "1234");

			/*
			 * String sql =
			 * "INSERT INTO employees (employee_name, employee_address, employee_level, employee_birthday) VALUES ("
			 * + newElement.getEmployeeName() + "," + newElement.getEmployeeAddress() + ","
			 * + newElement.getExperience() + "," + newElement.getDateOfBirth() + ")";
			 */

			String sql = "INSERT INTO employees (employee_name, employee_address, employee_level, employee_birthday) VALUES (?,?,?,?)";

			pStmt = con.prepareStatement(sql);
			pStmt.setString(1, newElement.getEmployeeName());
			pStmt.setString(2, newElement.getEmployeeAddress());
			pStmt.setInt(3, newElement.getExperience());
			pStmt.setDate(4, (java.sql.Date) newElement.getDateOfBirth());

			int rowsInserted = pStmt.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("(DAO) A new user was inserted successfully!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pStmt != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("(DAO) Insert operation completed successfully!");
	}

	@Override
	public void updateById(Employee newElement) {
		Connection con = null;
		PreparedStatement pStmt = null;

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeeregister", "root", "1234");

			String sql = "UPDATE employees SET employee_name=?, employee_address=?, employee_level=? WHERE employee_id="+newElement.getEmployeeId();
			
			pStmt = con.prepareStatement(sql);
			pStmt.setString(1, newElement.getEmployeeName());
			pStmt.setString(2, newElement.getEmployeeAddress());
			pStmt.setInt(3, (Integer)newElement.getExperience());
			
			int rowsInserted = pStmt.executeUpdate();
			if (rowsInserted > 0) {
				System.out.println("(DAO) A new user was updated successfully!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pStmt != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		System.out.println("(DAO) Update operation completed sucsessfully!");

	}

	@Override
	public void deleteById(Integer employeeId) {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeeregister", "root",
					"1234");

			String sql = "DELETE FROM employees WHERE employee_id =" + employeeId;

			PreparedStatement statement = con.prepareStatement(sql);

			int rowsDeleted = statement.executeUpdate();
			if (rowsDeleted > 0) {
				System.out.println("(DAO) An employee was deleted successfully!");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
