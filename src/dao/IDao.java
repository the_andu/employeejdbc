package dao;

import java.util.List;

public interface IDao<T> {

	public List<T> findAllEmployees();
	
	public T findById(Integer employeeId); 

	public void save(T newElement); 
	
	public void updateById(T newElement);
	
	public void deleteById(Integer employeeId);
}
