package model;

import java.util.Date;

public class Employee {

	private Integer employeeId; // MySQL (PK, NOT NULL, AUTO INCREMENT)
	private String employeeName;
	private String employeeAddress;
	private Integer experience; // levels 1 through 10
	private Date dateOfBirth;
	private Date dateOfJoining;

	public Employee() {
	}
	
	
	
	public Employee(String employeeName, String employeeAddress, Integer experience, Date dateOfBirth) {
		this.employeeName = employeeName;
		this.employeeAddress = employeeAddress;
		this.experience = experience;
		this.dateOfBirth = dateOfBirth;
	}



	public Employee(String employeeName, String employeeAddress, Date dateOfJoining, Integer experience,
			Date dateOfBirth) {
		this.employeeName = employeeName;
		this.employeeAddress = employeeAddress;
		this.dateOfJoining = dateOfJoining;
		this.experience = experience;
		this.dateOfBirth = dateOfBirth;
	}
	

	public Integer getEmployeeId() {
		return employeeId;
	}
	
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public String getEmployeeAddress() {
		return employeeAddress;
	}

	public void setEmployeeAddress(String employeeAddress) {
		this.employeeAddress = employeeAddress;
	}

	public Date getDateOfJoining() {
		return dateOfJoining;
	}

	public void setDateOfJoining(Date dateOfJoining) {
		this.dateOfJoining = dateOfJoining;
	}

	public Integer getExperience() {
		return experience;
	}

	public void setExperience(Integer experience) {
		this.experience = experience;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Override
	public String toString() {
		return "Employee [employeeID=" + employeeId + ", employeeName=" + employeeName + ", employeeAddress="
				+ employeeAddress + ", dateOfJoining=" + dateOfJoining + ", experience=" + experience + ", dateOfBirth="
				+ dateOfBirth + "]";
	}

}
