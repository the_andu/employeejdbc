package requirements;

public class JDBCApplication1 {
	
/*	Develop a JDBC application to do CRUD (Create, Read, Update, Delete) operation on Employee domain 
	with the fields 
	
	Employee Id Employee Name, 
	Employee Address, 
	Date of Joining, 
	Experience, 
	Date of Birth 
	
	using JDBC concept. 
	
	Use Junit testing for insert operation.
	
	Data type of the attributes
	•	Employee Id – Integer (Auto number)
	•	Employee Name – String
	•	Employee Address – String
	•	Date of Joining – Date 
	•	Experience – Integer
	•	Date of birth – Date
	
	
	Note: - 
	1)	Generate employee id while inserting the data.
	2)	Use employee id for the fetch and delete operation. 
	3)	While updating use employee id as reference.

	
*/

}