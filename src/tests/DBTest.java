package tests;

import java.util.List;

import dao.DaoEmployee;
import model.Employee;

public class DBTest {
	public static void main(String[] args) {
		
		DaoEmployee daoe = new DaoEmployee();
		// findAllEmployees() test
		List<Employee> empsInDb = daoe.findAllEmployees();
		
		System.out.println("Employees in Database: ");
		
		System.out.println("Number of employees found: " + empsInDb.size());
		for (Employee e : empsInDb) {
			System.out.println("Employees in DB: " + e.toString());
		}	// findAllEmployees() works, yay!
		
		// findById(Integer employeeId) test
		Employee emp1 = daoe.findById(1);
		System.out.println("Find by id test 1: " + emp1);
		Employee emp2 = daoe.findById(2);
		System.out.println("Find by id test 2: " + emp2);
			// findById(Integer employeeId) works, yay!
		
		// save(Employee newElement) test
		/*
		 * Employee emp4 = new Employee();
		 * 
		 * emp4.setEmployeeName("First Inserted Fellow");
		 * emp4.setEmployeeAddress("NoName Street x"); emp4.setExperience(6);
		 * emp4.setDateOfBirth(null);
		 * 
		 * daoe.save(emp4);
		 */
		/*
		 * Employee emp6 = new Employee("Third inserted fellow", "no place like home" ,
		 * 9, null); daoe.save(emp6);
		 */
		System.out.println("Saved to DB!");
			// save(Employee newElement) works, yay!
		
		// updateById(Integer employeeId) test
		/*
		 * Employee empToBeUpdated = daoe.findById(5);// new Employee();
		 * empToBeUpdated.setEmployeeName("Second Fellow Inserted");
		 * 
		 * daoe.updateById(empToBeUpdated);
		 */
		System.out.println("Employee details updated in DB!");
		    // updateById(Integer employeeId) works, yay!
		
		// deleteById(Integer employeeId) test
		// daoe.deleteById(1);
			// deleteById(Integer employeeId) works, yay!
	}
}
